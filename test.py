#!/usr/bin/python

import sys
import time
import yaml
import argparse
from pprint import pprint

config = None
app = None

parser = argparse.ArgumentParser()
parser.add_argument('--config')
parser.add_argument('check')
parser.add_argument('do')
args = parser.parse_args()

class color:
    H = '\033[95m'
    BL = '\033[94m'
    GR = '\033[92m'
    W = '\033[93m'
    F = '\033[1;36m'
    END = '\033[0m'
    BO = '\033[1m'
    UL = '\033[4m'
    G = '\033[1;30m'
    WH = '\033[36m'

def prnt(function_input):
    sys.stdout.write(function_input)

def progress_bar():
    start = "["
    end = "]"
    stick = "="
    for i in range(101):
        sticks = ''.join([stick] * i)
        prnt("{}{}{:99}{}\r".format(color.G, start, sticks, end))
        time.sleep(0.01)
    prnt('{}{:105}{}\n'.format(color.GR, '===> Done.', color.END))

def wait_bar(number):
    start = "["
    end = "]"
    carat = ">"
    stick = "="
    for r in range(int(number)):
        for i in range(101):
            sticks = ''.join([stick] * i)
            prnt("{}{}{:^100}{}\r".format(color.G, start, sticks, end))
            time.sleep(0.003)
        for i in range(100):
            sticks = ''.join([' '] * i)
            prnt("{}{}{:=^100}{}\r".format(color.G, start, sticks, end))
            time.sleep(0.003)
    prnt('{}{:105}{}\n'.format(color.GR, '===> Done.', color.END))

def load_config():
    global config
    with open(args.config, 'r') as c:
        config = yaml.load(c)


def args_check():
    global app
    app = args.check
    for key, value in config:
        if app == value['name']:
            pprint(value)
    sys.exit(0)

def args_do():
    global app
    app = args.do
    for a in config:
        if app == config[a]['name']:
            do_the_thing(config[a])
    print('App {} not found in config! Exiting.'.format(str(app)))
    sys.exit(0)

def do_the_thing(c):
    prnt('{}========> Packaging application :: {}{}\n'.format(color.F, config[0]['name'], color.END))
    prnt('Pulling latest source from {} at {} via {}\n'.format(c['scm']['branch'], c['scm']['url'], c['scm']['flavor']))
    wait_bar(1)
    prnt('Building app {} with command {}\n'.format(c['name'], c['build']['command']))
    wait_bar(1)
    prnt('{}========> DOCKER\n{}'.format(color.F, color.END))
    print '{:30}{}'.format('Setting ENTRYPOINT to:',c['docker']['entrypoint'])
    print '{:30}{}'.format('Exposing ports:', str(c['docker']['ports']))
    print '{:30}{}'.format('Using image:', c['docker']['image'])
    time.sleep(1)
    prnt('Generating Dockerfile\n')
    wait_bar(1)
    prnt('Building Docker image\n')
    wait_bar(1)
    prnt('Uploading Docker image to {}\n'.format(c['docker']['upload_url']))
    wait_bar(1)
    prnt('{}========> CLOUD{}\n'.format(color.F, color.END))
    cloud = c['cloud']
    print '{:30}{}'.format('Setting base image:', cloud['image'])
    print '{:30}{}'.format('Setting startup command:', cloud['start'])
    print '{:30}{}'.format('Using security groups:', str(cloud['security_groups']))
    print '{:30}{}'.format('Using keypair:', cloud['keypair'])
    for p in cloud['providers']:
        prnt('{}Provider :: {}{}\n'.format(color.W, p, color.END))
        image_name = cloud['image'] + '_' + config[0]['name'] + '_' + str(config[0]['version'] + '_' + p)
        prnt('Creating image {}\n'.format(image_name))
        wait_bar(1)
        prnt('Generating cloud-init script\n')
        wait_bar(1)
        prnt('Uploading image to {}\n'.format(p))
        wait_bar(1)
    prnt('{}========> Commiting config artifacts to {}{}\n'.format(color.F, config[0]['scm']['flavor'], color.END))
    wait_bar(1)
    prnt('{}========> Cleanup{}\n'.format(color.F, color.END))
    wait_bar(5)
    prnt('{}========> Application {} is complete.\n'.format(color.GR, config[0]['name'], color.END))
    sys.exit(0)

if __name__ == '__main__':
    prnt(color.GR)
    print '======== HEXAPP IMAGE BUILDER ========='
    prnt(color.G)
    print '(C) 2016 Hexadecimal Applications, Inc.'
    prnt(color.END)
    load_config()
    do_the_thing(config[0])
